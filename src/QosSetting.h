#ifndef QOSETTING_H
#define QOSETTING_H

#include <string>
#include <sstream>
#include <iostream>
using namespace std;

const long LOCAL_DURATION_INFINITE_SEC = (long)2147483647;
const signed long LOCAL_DURATION_INFINITE_NSEC = (signed long)2147483647UL;
const long LOCAL_LENGTH_UNLIMITED = (long)-1L;

class QosSetting
{
public:
  QosSetting();

  void setQosSetting(string line);

  int getReliableKind();
  int getOwnershipKind();
  int getDurabilityKind();
  long getDurabilityServiceSec();
  unsigned long getDurabilityServiceNsec();
  int getDurabilityServiceHistoryKind();
  long getDurabilityServiceHistoryDepth();
  long getDurabilityServiceMaxSamples();
  long getDurabilityServiceMaxInstances();
  long getDurabilityServiceMaxSamplesPerInstance();
  int getLivelinessKind();
  int getHistoryKind();
  int getDestinationOrderKind();
  long getLivelinessSec();
  unsigned long getLivelinessNsec();
  long getDeadlineSec();
  unsigned long getDeadlineNsec();
  long getHistoryDepth();
  long getResourceLimitSample();
  long getResourceLimitInstance();
  long getResourceLimitSamplePerInstance();
  long getLatencyBudgetSec();
  unsigned long getLatencyBudgetNsec();
  long getLifespanSec();
  unsigned long getLifespanNsec();
  long getTransportPriorityValue();
  string getTopicDataValue();

  // test
  void print_QosSetting();

private:
  int reliableKind;
  int ownershipKind;
  int durabilityKind;
  long durabilityServiceSec;
  unsigned long durabilityServiceNsec;
  int durabilityServiceHistoryKind;
  long durabilityServiceHistoryDepth;
  long durabilityServiceMaxSamples;
  long durabilityServiceMaxInstances;
  long durabilityServiceMaxSamplesPerInstance;
  int livelinessKind;
  int historyKind;
  int destinationOrderKind;
  long livelinessSec;
  unsigned livelinessNsec;
  long deadlineSec;
  unsigned deadlineNsec;
  long depth;
  long maxSample;
  long maxInstance;
  long maxInstancePerSample;
  long latencyBudgetSec;
  unsigned latencyBudgetNsec;
  long lifespanSec;
  unsigned lifespanNsec;
  long value;
  string topicDataValue;
};
#endif