#include "Timer.h"

using namespace std;

Timer::Timer(int cycle_time, function<void()> task)
{
	this->cycle_time = cycle_time;
	this->terminated = false;
	this->task = task;

	if (clock_gettime(CLOCK_REALTIME, &now) == -1)
	{
		cout << "error in gettime\n";
		exit(EXIT_FAILURE);
	}

	//cycle_time basic unit = 1 millisecond
	int sec = this->cycle_time / 1000;
	int msec = (this->cycle_time % 1000) * 1000000;
	timer.it_value.tv_sec = now.tv_sec + sec;
	timer.it_value.tv_nsec = now.tv_nsec + msec;
	timer.it_interval.tv_sec = sec;
	timer.it_interval.tv_nsec = msec;
}

void Timer::start()
{
	if (this->cycle_time != 0)
	{
		this->fd = timerfd_create(CLOCK_REALTIME, 0);
		if (this->fd == -1)
		{
			cout << "error in fd\n";
			exit(EXIT_FAILURE);
		}

		if (timerfd_settime(this->fd, TFD_TIMER_ABSTIME, &timer, NULL) == -1)
		{
			cout << "error in settime\n";
			exit(EXIT_FAILURE);
		}
	}

	t = new thread([this]() {
		uint64_t exp;
		while (!terminated)
		{
			if (cycle_time != 0)
			{
				ssize_t s = read(fd, &exp, sizeof(uint64_t));
				if (s != sizeof(uint64_t))
				{
					cout << "error in read\n";
					exit(EXIT_FAILURE);
				}
			}
			task();
		}
	});
}

void Timer::end()
{
	this->terminated = true;
	t->join();
	delete t;
}