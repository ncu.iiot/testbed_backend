#ifndef TIMER_H
#define TIMER_H

#include <thread>
#include <functional>
#include <chrono>
#include <vector>
#include <iostream>
#include <sys/timerfd.h>
#include <stdint.h>
#include <unistd.h>

using namespace std;

class Timer
{
  public:
	Timer(int cycle_time, function<void()> task);
	void start();
	void end();

  private:
	thread *t;
	int fd;
	int cycle_time;
	bool terminated;
	itimerspec timer;
	timespec now;
	function<void()> task;
};
#endif