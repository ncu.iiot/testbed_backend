#include "Topics.h"

#define REPORT_DIR "{TOPdir}/testbed/report/"

Topics::Topics()
{
  records.clear();
}

Topics::~Topics()
{
}

void Topics::setDeviceID(int deviceID)
{
  this->deviceID = deviceID;
}

void Topics::setLatency(string topicName, int deviceID, time_t sec, long nsec)
{
  insertRecord(topicName);
  timespec temp;
  temp.tv_sec = sec;
  temp.tv_nsec = nsec;
  this->records[topicName].latencies[deviceID].push_back(temp);
}

void Topics::addPubCount(string topicName)
{
  insertRecord(topicName);
  this->records[topicName].pubCount += 1;
}

void Topics::addSubCount(string topicName, int deviceID)
{
  insertRecord(topicName);
  insertSubConut(topicName, deviceID);
  this->records[topicName].subCount[deviceID] += 1;
}

bool Topics::addSubMsgID(string topicName, int deviceID, int msgID)
{
  insertRecord(topicName);
  if (!this->records[topicName].subMsgID[deviceID].count(msgID))
  {
    this->records[topicName].subMsgID[deviceID].insert(msgID);
    return true;
  }
  return false;
}

void Topics::insertRecord(string topicName)
{
  auto iter = records.find(topicName);

  if (iter == this->records.end())
  {
    Record temp;
    temp.pubCount = 0;
    temp.subCount.clear();
    temp.subMsgID.clear();
    temp.latencies.clear();
    this->records[topicName] = temp;
  }
}

void Topics::insertSubConut(string topicName, int deviceID)
{
  auto iter = records[topicName].subCount.find(deviceID);
  if (iter == this->records[topicName].subCount.end())
  {
    this->records[topicName].subCount[deviceID] = 0;
  }
}

void Topics::outputFile(string deviceName)
{
  string path = REPORT_DIR;
  string subTitle = ".log";
  for (auto record : records)
  {
    // get log file name
    string topicName = record.first;
    string filename = path + deviceName + "_" + topicName + subTitle;
    // save record to json
    Json::Value logRoot;
    // publish count
    logRoot["pubCount"] = getPubCount(topicName);
    // subscribe count
    Json::Value subCount;
    for (auto sub : getSubCount(topicName))
    {
      subCount[to_string(sub.first)] = sub.second;
    }
    logRoot["subCount"] = subCount;
    // subscribe message id
    Json::Value subMsgID;
    for (auto sub : getSubMsgID(topicName))
    {
      Json::Value msgIDs;
      int max = sub.second.size();
      for (auto id : sub.second)
      {
        msgIDs.append(id);
      }
      subMsgID[to_string(sub.first)] = msgIDs;
    }
    logRoot["subMsgID"] = subMsgID;
    // latency
    Json::Value latencies;
    vector<int> latencyIndices = getLatencyIndices(topicName);
    for (auto latencyIndex : latencyIndices)
    {
      Json::Value device_latency;
      auto latency = getLatencyAt(topicName, latencyIndex);
      int max = latency.size();
      for (int i = 0; i < max; i++)
      {
        stringstream ss;
        ss << latency[i].tv_sec;
        ss << "." << setfill('0') << setw(9) << latency[i].tv_nsec;
        device_latency.append(stod(ss.str()));
      }
      latencies[to_string(latencyIndex)] = device_latency;
    }
    logRoot["latency"] = latencies;
    // create log file
    fstream file;
    file.open(filename.c_str(), ios::out);
    // write to log file
    if (!file)
    {
      cout << "Fail to open file: " << filename << endl;
    }
    else
    {
      file << logRoot.toStyledString() << endl;
    }
    file.close();
  }
}

int Topics::getDeviceID()
{
  return deviceID;
}

int Topics::getPubCount(string topicName)
{
  auto temp = records.find(topicName);

  if (temp != this->records.end())
    return this->records[topicName].pubCount;
  else
    return 0;
}

map<int, int> Topics::getSubCount(string topicName)
{
  auto temp = records.find(topicName);
  map<int, int> empty;

  if (temp != this->records.end())
    return this->records[topicName].subCount;
  else
    return empty;
}

map<int, set<int>> Topics::getSubMsgID(string topicName)
{
  auto temp = records.find(topicName);
  map<int, set<int>> empty;

  if (temp != this->records.end())
    return this->records[topicName].subMsgID;
  else
    return empty;
}

int Topics::getLatencySize(string topicName)
{
  auto temp = records.find(topicName);

  if (temp != this->records.end())
    return this->records[topicName].latencies.size();
  else
    return 0;
}

vector<int> Topics::getLatencyIndices(string topicName)
{
  vector<int> temp;
  for (auto lantency : this->records[topicName].latencies)
    temp.push_back(lantency.first);

  return temp;
}

vector<timespec> Topics::getLatencyAt(string topicName, int latencyIndex)
{
  return this->records[topicName].latencies[latencyIndex];
}
