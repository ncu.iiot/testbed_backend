#ifndef DEVICE_H
#define DEVICE_H

#include <vector>
using namespace std;

class Device
{
public:
  Device();
  ~Device();

  void setCPU(double cpu);
  double getCPUAt(int index);

  void setMem(double mem);
  double getMemAt(int index);

  void setInBandwidth(int bandwidth);
  int getInBandwidthInfoAt(int index);

  void setOutBandwidth(int bandwidth);
  int getOutBandwidthInfoAt(int index);

  int getDeviceSize();

private:
  vector<double> cpuInfo;
  vector<double> memInfo;
  vector<int> inBandwidthInfo;
  vector<int> outBandwidthInfo;
};
#endif