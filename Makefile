SHELL=/bin/bash

TOPdir=${HOME}
ETH0=ens3

PROJNAME=emulation
BUILDdir=build
AWdir=agentworker
RELEASEdir=release

all: create_build_env replace_topdir replace_eth0 $(PROJNAME)_types $(PROJNAME) release

clean depend generated realclean check-syntax $(CUSTOM_TARGETS):
	rm -rf build
	rm -rf $(RELEASEdir)

.PHONY: $(PROJNAME)_types
$(PROJNAME)_types:
	cd $(BUILDdir) && $(MAKE) -f ../Makefile.$(PROJNAME)_types all

.PHONY: $(PROJNAME)
$(PROJNAME): $(PROJNAME)_types
	cd $(BUILDdir) && $(MAKE) -f ../Makefile.$(PROJNAME) all

create_build_env:
	mkdir -p $(BUILDdir)
	cp -a src/* $(BUILDdir)/
	cp -a agentworker $(BUILDdir)/
	cp -a Makefile.install $(BUILDdir)/

replace_topdir:
	cd $(BUILDdir) && sed -i 's/{TOPdir}/$(subst /,\/,${TOPdir})/g' agentworker/agent-wrap.sh
	cd $(BUILDdir) && sed -i 's/{TOPdir}/$(subst /,\/,${TOPdir})/g' agentworker/testbed-agent.service
	cd $(BUILDdir) && sed -i 's/{TOPdir}/$(subst /,\/,${TOPdir})/g' agentworker/config.py
	cd $(BUILDdir) && sed -i 's/{TOPdir}/$(subst /,\/,${TOPdir})/g' Emulation.cpp
	cd $(BUILDdir) && sed -i 's/{TOPdir}/$(subst /,\/,${TOPdir})/g' Topics.cpp
	cd $(BUILDdir) && sed -i 's/{TOPdir}/$(subst /,\/,${TOPdir})/g' Makefile.install

replace_eth0:
	cd $(BUILDdir) && sed -i 's/{ETH0}/$(subst /,\/,${ETH0})/g' Emulation.cpp

release:
	mkdir -p $(RELEASEdir)/lib
	cp -p INSTALL.md $(RELEASEdir)/
	cp -a $(BUILDdir)/agentworker $(RELEASEdir)/
	cp -p $(BUILDdir)/Makefile.install $(RELEASEdir)/Makefile
	cp -p $(BUILDdir)/$(PROJNAME) $(RELEASEdir)/emulation
	cp -p $(BUILDdir)/*.so $(RELEASEdir)/lib
	chmod +x $(RELEASEdir)/agentworker/agent-wrap.sh
	chmod +x $(RELEASEdir)/emulation
