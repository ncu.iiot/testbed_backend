'''
This program is used to emulate a device, it can send and receive the message by DDS application.
Version Testbed 2.0
Date 2018 05 14
Author Luko
'''

import json
import redis
import time
import socket
import subprocess
import glob
import os
import threading
import signal
import warnings
from config import config
from pprint import pprint
from StringIO import StringIO
from qos import Qos


EXPERIMENT_START = '2'
EXPERIMENT_END = '3'
EXPERIMENT_EXCEPTION = '4'
EXPERIMENT_ABORT = '5'


def get_iptable():
    get_s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    get_s.connect(('8.8.8.8', 0))

    ip_table = 'ip:%s' % get_s.getsockname()[0]

    return ip_table


def update_redis_time(redis):
    now = []
    now = redis.time()
    ip_table = '%s' % get_iptable()

    # now[0] = unix time in seconds, now[1] = microseconds
    redis.hset(ip_table, 'time', '%s' % now[0])


def heartbeat(redis):
    try:
        while stop_agentworker(redis) is False:
            update_redis_time(redis)
            time.sleep(1)

    except Exception as e:
        print 'heartbeat error:', (e)


def experiment_start(redis):
    ip_table = '%s' % get_iptable()
    return bool(redis.hget(ip_table, 'worker_start'))


def load_redis(redis):
    device_setting = ''
    topic_settings = []
    experiment_time = '30'

    try:
        ip_table = '%s' % get_iptable()
        device_setting = json.loads(redis.hget(ip_table, 'device_setting'))
        topic_settings = json.loads(redis.hget(ip_table, 'topic_setting'))
        device_id = int(redis.hget(ip_table, 'device_id'))
        experiment_time = redis.get('experiment_time')
    except Exception as e:
        print 'load_redis error:', (e)

    return device_setting, topic_settings, device_id, experiment_time


def save_data_setting(device_setting, topic_settings, device_id, experiment_time):
    output = ''

    output += '%s ' % device_setting.get('deviceName')
    output += '%d ' % device_id
    output += '%d ' % len(device_setting.get('pubSetting'))
    output += '%d ' % len(device_setting.get('subSetting'))
    output += '%s ' % experiment_time
    output += '%s\n' % len(topic_settings)

    for pubSetting in device_setting.get('pubSetting'):
        output += '%s %s ' % (pubSetting.get('topicName'),
                              int(pubSetting.get('dataSize')))
        output += '%s\n' % pubSetting.get('dataFrequency')

    for subSetting in device_setting.get('subSetting'):
        output += '%s %s\n' % (subSetting.get('topicName'),
                               subSetting.get('save'))

    for topic_setting in topic_settings:
        qos_setting = topic_setting.get('qosSetting')
        qos = Qos(qos_setting)
        output += qos.get_reliability()
        output += qos.get_ownership()
        output += qos.get_durability()
        output += qos.get_liveliness()
        output += qos.get_deadline()
        output += qos.get_history()
        output += qos.get_resource_limit()
        output += qos.get_latency_budget()
        output += qos.get_lifespan()
        output += qos.get_destination_order()
        output += qos.get_transport_priority()
        output += qos.get_durability_service()
        output += qos.get_topic_data()
        output += '\n'

    # output += str(device_setting)

    f = open(config.DATASETTING_PATH, 'w')
    f.write(output)
    f.close()


def set_worker_is_ready(redis):
    redis.hset('%s' % get_iptable(), 'worker_ready', '1')


def reset_worker_is_ready(redis):
    redis.hset('%s' % get_iptable(), 'worker_ready', '')


def dds_started(redis):
    experiment_status = redis.get('experiment_status')
    if experiment_status is EXPERIMENT_START:
        return True
    elif experiment_status is EXPERIMENT_END:
        return False
    elif experiment_status is EXPERIMENT_EXCEPTION:
        return False
    elif experiment_status is EXPERIMENT_ABORT:
        return False
    return False


def execute_dds(redis):
    cmd = ''
    cmd += config.DDS_PATH
    result = True
    is_start = False
    remove_dds_ready()
    remove_dds_start()

    print "wait for releasing resource"
    time.sleep(30)

    try:
        my_env = os.environ.copy()
        proc = subprocess.Popen(
            cmd, shell=False, env=my_env, preexec_fn=os.setsid)

        # process end, process = None
        while proc.poll() == None:
            if check_dds_ready():
                set_worker_is_ready(redis)
                os.remove(config.DDS_READY_PATH)
            if dds_started(redis) and not is_start:
                set_dds_start()
                reset_worker_is_ready(redis)
                is_start = True
            if get_experiment_status(redis) == EXPERIMENT_ABORT:
                set_worker_is_ready(redis)
                proc.terminate()
                proc.wait()

                try:
                    os.killpg(proc.pid, signal.SIGKILL)
                except OSError as e:
                    warnings.warn(e)

                result = False
                break
            time.sleep(1)

        return result
    except Exception as e:
        print 'execute_dds() error:', e


def check_dds_ready():
    return os.path.isfile(config.DDS_READY_PATH)


def remove_dds_ready():
    if os.path.isfile(config.DDS_READY_PATH):
        os.remove(config.DDS_READY_PATH)


def set_dds_start():
    with file(config.DDS_START_PATH, "w") as f:
        f.write("start\n")


def remove_dds_start():
    if os.path.isfile(config.DDS_START_PATH):
        os.remove(config.DDS_START_PATH)


def reset_redis_talbe(redis):
    ip_table = '%s' % get_iptable()
    redis.hset(ip_table, 'worker_start', '')
    redis.hset(ip_table, 'worker_done', '1')


def stop_agentworker(redis):
    return bool(redis.get('stop_agentworker'))


def init_table(redis):
    talbe_name = get_iptable()

    redis.set('stop_agentworker', '')
    redis.hsetnx(talbe_name, 'device_setting', '')
    redis.hset(talbe_name, 'worker_start', '')
    redis.hset(talbe_name, 'worker_ready', '')
    redis.hset(talbe_name, 'worker_done', '')
    redis.hset(talbe_name, 'time', '')


def wait_for_experiment_end(redis):
    while True:
        result = redis.get('experiment_status')
        if result is EXPERIMENT_END:
            break
        elif result is EXPERIMENT_EXCEPTION:
            break
        elif result is EXPERIMENT_ABORT:
            break
        time.sleep(1)


def submit_result_by_ssh():
    cmd = 'cd %s && ' % config.REPORT_PATH
    cmd = cmd + 'scp *.log ' + config.SERVER_USER_NAME + \
        '@' + config.SERVER_IP + ':' + config.FRONTEND_REPORT_PATH
    os.system(cmd)
    pass


def remove_old_log():
    try:
        delete_path = '%s*' % config.REPORT_PATH
        for log in glob.glob(delete_path):
            if os.path.exists(log):
                os.remove(log)
    except Exception as e:
        print 'remove_old_report() error:', e
    pass


def get_experiment_status(redis):
    return redis.get('experiment_status')


def main():
    print 'Agent worker start'

    r = redis.StrictRedis(
        host=config.SERVER_IP, port=config.REDIS_PORT, password=config.REDIS_PASSWORD)

    try:
        init_table(r)
        thread_heartbeat = threading.Thread(
            target=heartbeat, name='heartbeat', args=(r,))
        thread_heartbeat.daemon = True
        thread_heartbeat.start()

        while stop_agentworker(r) is not True:
            if experiment_start(r) is False:
                print 'wait'
            else:
                remove_old_log()
                device_setting, topic_settings, device_id, experiment_time = load_redis(
                    r)
                save_data_setting(device_setting, topic_settings,
                                  device_id, experiment_time)

                if execute_dds(r) is True:
                    submit_result_by_ssh()

                reset_redis_talbe(r)
                wait_for_experiment_end(r)

            time.sleep(1)

    except Exception as e:
        print 'main error:', e

    pass


if __name__ == '__main__':
    main()
