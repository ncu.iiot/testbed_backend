class Qos:
    def __init__(self, qos_setting):
        self.qos_setting = qos_setting

    def __get_duration_time(self, sec, nsec):
        output = ''

        if sec is '':
            output += '0 '
        elif long(sec) > 0:
            output += '%d ' % long(sec)
        else:
            output += '0 '

        if nsec is '':
            output += '0 '
        elif long(nsec) > 0:
            output += '%d ' % long(nsec)
        else:
            output += '0 '

        return output

    def __get_single_value(self, value, default_value):
        output = ''

        if value == '':
            output += '%d ' % long(default_value)
        elif long(value) > 0:
            output += '%d ' % long(value)
        else:
            output += '%d ' % long(default_value)

        return output

    def get_reliability(self):
        reliability_setting = self.qos_setting.get('reliability')
        reliability_kind = reliability_setting.get('kind')
        # print 'type of reliability_kind:', type(reliability_kind)
        output = '1 '

        if reliability_kind == unicode('best_effort'):
            output = '1 '
        elif reliability_kind == unicode('reliable'):
            output = '2 '

        return output

    def get_ownership(self):
        ownership_setting = self.qos_setting.get('ownership')
        ownership_kind = ownership_setting.get('kind')
        output = '1 '

        if ownership_kind == unicode('shared'):
            output = '1 '
        elif ownership_kind == unicode('exclusive'):
            output = '2 '

        return output

    def get_durability(self):
        durability_setting = self.qos_setting.get('durability')
        durability_kind = durability_setting.get('kind')
        output = '1 '

        if durability_kind == unicode('volatile'):
            output = '1 '
        elif durability_kind == unicode('transient_local'):
            output = '2 '
        elif durability_kind == unicode('transient'):
            output = '3 '
        elif durability_kind == unicode('persistent'):
            output = '4 '

        return output

    def get_liveliness(self):
        liveliness_setting = self.qos_setting.get('liveliness')
        liveliness_kind = liveliness_setting.get('kind')
        liveliness_sec = liveliness_setting.get('leaseDurationSec')
        liveliness_nsec = liveliness_setting.get('leaseDurationNsec')
        output = ''

        if liveliness_kind == unicode('automic'):
            output += '1 '
        elif liveliness_kind == unicode('manual_by_particpant'):
            output += '2 '
        elif liveliness_kind == unicode('manual_by_topic'):
            output += '3 '
        else:
            output += '1 '

        output += self.__get_duration_time(liveliness_sec, liveliness_nsec)

        return output

    def get_deadline(self):
        deadline_setting = self.qos_setting.get('deadline')
        deadline_sec = deadline_setting.get('periodSec')
        deadline_nsec = deadline_setting.get('periodNsec')
        output = ''
        output += self.__get_duration_time(deadline_sec, deadline_nsec)

        return output

    def get_history(self):
        history_setting = self.qos_setting.get('history')
        kind = history_setting.get('kind')
        depth = history_setting.get('depth')
        output = ''

        if kind == unicode('keepLastHistory'):
            output += '1 '
        elif kind == unicode('keepAllHistory'):
            output += '2 '
        else:
            output += '1 '

        output += self.__get_single_value(depth, 1)

        return output

    def get_resource_limit(self):
        resource_limit_setting = self.qos_setting.get('resourceLimit')
        settings = []
        settings.append(resource_limit_setting.get('maxSamples'))
        settings.append(resource_limit_setting.get('maxInstances'))
        settings.append(resource_limit_setting.get('maxSamplesPerInstance'))
        output = ''

        for setting in settings:
            output += self.__get_single_value(setting, 0)

        return output

    def get_latency_budget(self):
        latency_budget_setting = self.qos_setting.get('latencyBudget')
        latency_budget_sec = latency_budget_setting.get('durationSec')
        latency_budget_nsec = latency_budget_setting.get('durationNsec')
        output = ''
        output += self.__get_duration_time(latency_budget_sec,
                                           latency_budget_nsec)

        return output

    def get_lifespan(self):
        lifespan_setting = self.qos_setting.get('lifespan')
        lifespan_sec = lifespan_setting.get('durationSec')
        lifespan_nsec = lifespan_setting.get('durationNsec')
        output = ''
        output += self.__get_duration_time(lifespan_sec, lifespan_nsec)

        return output

    def get_destination_order(self):
        destination_order_setting = self.qos_setting.get('destinationOrder')
        destination_order_kind = destination_order_setting.get('kind')
        output = ''

        if destination_order_kind == unicode('byReceptionTimestamp'):
            output += '1 '
        elif destination_order_kind == unicode('bySourceTimestamp'):
            output += '2 '
        else:
            output += '1 '

        return output

    def get_transport_priority(self):
        transport_priority_setting = self.qos_setting.get('transportPriority')
        transport_priority_value = transport_priority_setting.get('value')
        output = ''
        output += self.__get_single_value(transport_priority_value, 0)

        return output

    def get_durability_service(self):
        durability_service_setting = self.qos_setting.get('durabilityService')
        cleanup_delay_sec = durability_service_setting.get(
            'serviceCleanupDelaySec')
        cleanup_delay_nsec = durability_service_setting.get(
            'serviceCleanupDelayNsec')
        history_kind = durability_service_setting.get('historyKind')
        settings = []
        settings.append(durability_service_setting.get('historyDepth'))
        settings.append(durability_service_setting.get('maxSamples'))
        settings.append(durability_service_setting.get('maxInstances'))
        settings.append(durability_service_setting.get(
            'maxSamplesPerInstance'))

        output = ''
        output += self.__get_duration_time(cleanup_delay_sec,
                                           cleanup_delay_nsec)

        if history_kind == unicode('keepLastHistory'):
            output += '1 '
        elif history_kind == unicode('keepAllHistory'):
            output += '2 '
        else:
            output += '1 '

        for setting in settings:
            output += self.__get_single_value(setting, 0)

        return output

    def get_topic_data(self):
        topic_data_setting = self.qos_setting.get('topicData')
        value = topic_data_setting.get('value')
        output = '%s ' % value.replace(' ', '_')
        return output
