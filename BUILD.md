# IIOT Testbed 2.1

## Build from source

### Build Steps
- Check Build Dependencies
  - DDS
  - Ubuntu Packages
- Configure Build Options 
- Build Testbed Backend

### OS Requirement
OS | VERSION | BIT
:-|:-:|:-:
Ubuntu | 16.04 | 32

### Check Build Dependencies

#### DDS
Vortex OpenSplice 6.9.0

#### Ubuntu Packages
Package | Version
-|-
build-essential | 12.1ubuntu2
libjsoncpp-dev | 1.7.2-1

To install those packages, do:
```bash
sudo apt install build-essential libjsoncpp-dev
```

### Configure Build Options: 

Please modify these options in **Makefile** before you build:
- TOPdir : Top directory of Testbed in runtime.
- ETH0 : Network interface used by DDS.

Example:
```Makefile
TOPdir=${HOME}
ETH0=ens3
```

### Build Testbed Backend

```bash
# setup OpenSplice runtime environment, please replace <OpenSplice-release.com> to correct path
source <Vortex_OpenSplice_release.com>
make
```

After build finished, **release** folder and **build** folder will be created.

Contents in **build** folder, including:
- all files needed in build phase.
- object files created after build.

Contents in **release** folder, including:
- emulation : a program used to emulate DDS communication
- agentworker : a service used to receive job from Testbed frontend and do emulation.
- Makefile : a file used to install/uninstall Testbed backend

## Remove Build results
Remove **build** folder and **release** folder

```bash
make clean
```
